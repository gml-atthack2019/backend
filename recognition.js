const speech = require("@google-cloud/speech").v1p1beta1;
const admin = require("./config/firebase");

const { Transform } = require("stream");

let db = admin.firestore();

module.exports = (stream, roomname, userid, channelid) => {
  let roomdoc = db.collection("rooms").doc(roomname);
  const speakerDetectionEnabled = roomname == "444";

  console.log("Starting client with stream");
  const client = new speech.SpeechClient();

  const encoding = "LINEAR16";
  const sampleRateHertz = 8000;
  const languageCode = "en-US";

  const request = {
    config: {
      enableWordTimeOffsets: true,
      encoding: encoding,
      sampleRateHertz: sampleRateHertz,
      languageCode: languageCode,
      max_alternatives: 1,
      enableSpeakerDiarization: speakerDetectionEnabled,
      diarizationSpeakerCount: 2,
      enableAutomaticPunctuation: true,
      enableWordConfidence: false,
      model: speakerDetectionEnabled ? `phone_call` : `video`
    },
    interimResults: true
  };

  let ended = false;
  let streamingLimit = 60000;
  let recognizeStream = null;
  let restartCounter = 0;
  let audioInput = [];
  let lastAudioInput = [];
  let resultEndTime = 0;
  let totalEndTime = 0;
  let isFinalEndTime = 0;
  let finalRequestEndTime = 0;
  let newStream = true;
  let bridgingOffset = 0;
  let lastResultTime = 0;
  let lastTranscriptWasFinal = false;
  let wordsInSession = {};

  const resultData = async data => {
    resultEndTime =
      data.results[0].resultEndTime.seconds * 1000 +
      Math.round(data.results[0].resultEndTime.nanos / 1000000);

    var message = {
      userid,
      channelid,
      text: data.results[0].alternatives[0].transcript,
      confidence: data.results[0].alternatives[0].confidence,
      date: new Date()
    };
    let transcriptions = (await roomdoc.get()).data().transcriptions;
    if (!transcriptions) {
      transcriptions = [];
    }
    let updated = false;
    if (data.results[0].isFinal) {
      // Clear transcription
      transcriptions = transcriptions.filter(it => it.userid != message.userid);
    } else {
      transcriptions = transcriptions.map(it => {
        if (it.userid == message.userid) {
          updated = true;
          return message;
        }
        return it;
      });
      if (!updated) {
        transcriptions.push(message);
      }
    }
    if (data.results[0].isFinal) {
      console.log("ResultEndTime: " + resultEndTime);
      var wordsBySpeaker = {};
      var wordsNew = data.results[0].alternatives[0].words.filter(it => {
        var thisStartTime =
          it.startTime.seconds * 1000 +
          Math.round(it.startTime.nanos / 1000000);
        return thisStartTime >= isFinalEndTime;
      });
      wordsNew.forEach(it => {
        if (!wordsBySpeaker[it.speakerTag]) {
          wordsBySpeaker[it.speakerTag] = [];
        }
        wordsBySpeaker[it.speakerTag].push(it);
      });
      var newMessages = [];
      if (speakerDetectionEnabled) {
        Object.keys(wordsBySpeaker).forEach(key => {
          newMessages.push({
            userid: userid + ":" + key,
            channelid,
            text: wordsBySpeaker[key].map(it => it.word).join(" "),
            confidence: 1,
            date: new Date(),
            startTime: wordsBySpeaker[key][0].startTime.seconds,
            endTime:
              wordsBySpeaker[key][wordsBySpeaker[key].length - 1].endTime
                .seconds
          });
        });
      } else {
        console.log(data.results[0]);
        newMessages.push({
          startTime:
            data.results[0].alternatives[0].words &&
            data.results[0].alternatives[0].words[0]
              ? Number(totalEndTime) +
                Number(
                  data.results[0].alternatives[0].words[0].startTime.seconds
                )
              : null,
          endTime:
            Number(totalEndTime) +
            Number(data.results[0].resultEndTime.seconds),
          ...message
        });
      }
      console.log(newMessages);
      roomdoc.update({
        transcriptions: transcriptions,
        messages: admin.firestore.FieldValue.arrayUnion(...newMessages)
      });
      isFinalEndTime = resultEndTime;
    } else {
      roomdoc.update({
        transcriptions: transcriptions
      });
    }
    lastTranscriptWasFinal = data.results[0].isFinal;
  };

  const audioInputStreamTransform = new Transform({
    transform: (chunk, encoding, callback) => {
      if (newStream && lastAudioInput.length !== 0) {
        // Approximate math to calculate time of chunks
        const chunkTime = streamingLimit / lastAudioInput.length;
        if (chunkTime !== 0) {
          if (bridgingOffset < 0) {
            bridgingOffset = 0;
          }
          if (bridgingOffset > finalRequestEndTime) {
            bridgingOffset = finalRequestEndTime;
          }
          const chunksFromMS = Math.floor(
            (finalRequestEndTime - bridgingOffset) / chunkTime
          );
          bridgingOffset = Math.floor(
            (lastAudioInput.length - chunksFromMS) * chunkTime
          );

          for (let i = chunksFromMS; i < lastAudioInput.length; i++) {
            //recognizeStream.write(lastAudioInput[i]);
          }
        }
        newStream = false;
      }

      audioInput.push(chunk);

      if (recognizeStream) {
        recognizeStream.write(chunk);
      }

      callback();
    }
  });

  const startStream = () => {
    audioInput = [];
    recognizeStream = client
      .streamingRecognize(request)
      .on("error", err => {
        if (err.code === 11) {
          // restartStream();
        } else {
          console.error("API request error " + err);
        }
      })
      .on("data", resultData);
    setTimeout(restartStream, streamingLimit);
  };

  const restartStream = () => {
    if (ended) {
      return;
    }
    if (recognizeStream) {
      recognizeStream.removeListener("data", resultData);
      recognizeStream.end();
      recognizeStream = null;
    }
    if (resultEndTime > 0) {
      finalRequestEndTime = isFinalEndTime;
    }
    totalEndTime += resultEndTime / 1000;
    resultEndTime = 0;

    lastAudioInput = [];
    lastAudioInput = audioInput;

    restartCounter++;
    console.log("Restarting stream");
    newStream = true;

    startStream();
  };

  stream.pipe(audioInputStreamTransform);
  startStream();

  return {
    endStream: () => {
      console.log("Ending recognition");
      recognizeStream.removeListener("data", resultData);
      recognizeStream.end();
      recognizeStream = null;
      ended = true;
    }
  };
};
