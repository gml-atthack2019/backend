# Backend

This is the backend 

It consists of multiple parts:

- Asterisk ARI connector
- Stream processor
- Speech recognition
- Recordings server

## Installation

### API setup

You will need to set up a Google Cloud Platform project and enable access to the Speech-to-Text API. 
Then generate a service account associated with that project and download it's private key. 
Save the file as `speechcred.json` and run the command `source env.sh` to load it as an environment variable. 

### Environment

As this is a Node.js project, so first you need to make sure you have it installed.
You will need to have Asterisk running, with the configuration provided in the [asterisk-configs](https://gitlab.com/gml-atthack2019/asterisk-configs) repo.

After that's set up, install the required dependencies with: 

```
npm install
```

You can finally run it with the command

```
npm start
```