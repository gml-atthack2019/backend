const ariClient = require("ari-client");
const fs = require("fs");
const tailStream = require("jimbly-tail-stream");
const admin = require("./config/firebase");

var conferences = {};

let db = admin.firestore();

ariClient
  .connect("http://localhost:8088", "ariuser", "atthack")
  .then(function(ari) {
    console.log("Connected");
    async function channelJoined(event, channel) {
      console.log(event.channel);
      var number = event.channel.dialplan.exten;
      var user = event.channel.caller.number;
      var roomdoc = db.collection("rooms").doc(number);
      var roomdata = await roomdoc.get();
      if (!roomdata.exists) {
        roomdoc.set({ transcriptions: [], messages: [], users: [] });
      }
      await channel.answer();

      var readstream = tailStream.createReadStream(
        "/var/spool/asterisk/monitor/rec-" + channel.id + "-in.wav",
        { interval: 20 }
      );

      var recognition = require("./recognition")(
        readstream,
        number,
        user,
        channel.id
      );

      if (!conferences[number]) {
        console.log("Created new bridge");
        var bridge = await ari.Bridge().create({ type: "mixing" });
        bridge.on("ChannelEnteredBridge", (event, objects) => {
          console.log("ChannelEnteredBridge");
        });
        bridge.on("ChannelLeftBridge", (event, objects) => {
          console.log("ChannelLeftBridge");
          setTimeout(async () => {
            let channelsList = (
              await ari.bridges.get({ bridgeId: objects.bridge.id })
            ).channels;
            if (channelsList.length === 0) {
              console.log("Bridge abandoned");
              delete conferences[number];
              objects.bridge.destroy();
            }
          }, 2000);
        });
        conferences[number] = {
          bridge,
          channels: []
        };
      }

      setTimeout(async () => {
        conferences[number].channels.push(channel.id);
        var bridgeAdd = await ari.bridges.addChannel({
          bridgeId: conferences[number].bridge.id,
          channel: conferences[number].channels
        });
        channel.play({ media: "sound:hello-world" }, ari.Playback());
        console.log(
          (await ari.bridges.get({ bridgeId: conferences[number].bridge.id }))
            .channels
        );
        roomdoc.update({
          users: admin.firestore.FieldValue.arrayUnion(user)
        });
      }, 1000);

      channel.on("StasisEnd", async () => {
        console.log("Call ended");
        recognition.endStream();
        readstream.close();
        roomdoc.update({
          users: admin.firestore.FieldValue.arrayRemove(user)
        });
      });
    }

    ari.on("StasisStart", channelJoined);
    ari.start("conference");
  })
  .catch(function(err) {
    console.log("Can't connect to Asterisk");
  });
