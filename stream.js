const WebSocket = require("ws");
const wss = new WebSocket.Server({ port: 8080 });

const { spawn } = require("child_process");

module.exports = (callback) => {
  wss.on("connection", function connection(ws) {
    console.log("Got connection ");
    const cp = spawn("sox", ["-r", "8000", "-e", "signed-integer", "-b", "16", "-t", "raw", "-", "-t", "wav", "-"], { encoding: 'binary', stdio: 'pipe' });
    const stream = cp.stdout;
    callback(stream);
    ws.on("message", function incoming(message) {
      cp.stdin.write(message);
    });
  });
};
