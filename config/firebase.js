const admin = require("firebase-admin");
var serviceAccount = require("./firebase.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://atthack2019.firebaseio.com"
});
module.exports = admin;
